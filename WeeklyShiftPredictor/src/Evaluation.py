# -*- coding: utf-8 -*-
# !/bin/env python

import pytz
import yaml
import logging

from datetime import datetime
from dataframeBuilder import *
from iotools.apiDataCatcher import *
from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import mean_squared_error


def eval_auc(df):
    try:
        return roc_auc_score(df['real_area'], df['pred_area'])
    except:
        return None


def eval_rmse(df):
    try:
        return mean_squared_error(df['real'], df['pred'])**0.5
    except:
        return None


class Evaluator:

    """
    Get the predictions done by hour and the real demand that had occurred
    and evaluate based on AUC (area coverage) and RMSE (prediction accuracy)
    """

    def __init__(self, transports, conf_path, start_at, end_at):
        # detect configuration
        try:
            with open(conf_path) as conf_f:
                self.cfg = yaml.load(conf_f)
        except:
            print('Configuration (YAML) File Not Found')
            sys.exit()
        # variables
        self.startat = start_at
        self.endat = end_at
        self.area = self.cfg['general']['area_type']
        self.tz = pytz.timezone(self.cfg['general']['timezone'])
        self.transports = transports
        self.log = self.setup_logger()
        # data
        self.realData, self.hPred = self.retrive_all_historical_data()

    def setup_logger(self):
        """
        configure logging system
        """
        self.cur_date = datetime.now().replace(minute=00, second=00)\
            .strftime('%Y-%m-%d')
        # set log filename
        LOG_FILENAME = self.cfg['evaluator']['log_path']\
            .replace('DATE', self.cur_date)\
            .replace('AREA', self.area)
        # set up logging to file - see previous section for more details
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(funcName)-32s %(message)s',
            datefmt='%m-%d %H:%M',
            filename=LOG_FILENAME,
            filemode='w')
        # Set up a specific logger
        logger = logging.getLogger()
        return logger

    def retrive_all_historical_data(self):
        """
        makes common queries for all transport type combined in order
        to save time and improve performances
        """
        print('Retrieving Historical Data')
        tt = self.transports[0]
        self.log.info('** Getting dates from %s transport type' % tt)
        # define start and end date
        rec_isod_obj = dateutil.parser.parse(self.startat)
        now_isod_obj = dateutil.parser.parse(self.endat)
        # Check type of data request
        s2level = self.s2level if self.area == 'cell' else None
        # call TT API to get new data
        ttapi = AllTTDataAPICatcher(
            log=self.log,
            area=self.area,
            city=self.cfg['general']['city'],
            s2level=s2level,
            conf=self.cfg['evaluator']['api'][self.area]['get_feat_data'])
        # tt -> isodate -> area
        realData = ttapi.load_new_data_for_all_tt(rec_isod_obj, now_isod_obj)
        # reorganize dictionary
        for tt in realData:
            for ts in realData[tt]:
                for area in realData[tt][ts]:
                    realData[tt][ts][area] = realData[tt][ts][area]['demand']
        # call TT API to get historical predictions
        ttapi = AllTTDataAPICatcher(
            log=self.log,
            area=self.area,
            city=self.cfg['general']['city'],
            s2level=s2level,
            conf=self.cfg['evaluator']['api'][self.area]['get_prediction'])
        # tt -> isodate -> area
        hPred = ttapi.load_predictions_for_all_tt(rec_isod_obj, now_isod_obj)
        #
        return realData, hPred

    def build_key_lists(self):
        l_isodate = set()
        l_area = set()
        l_tt = set()
        for tt in self.realData:
            l_tt.add(tt)
            for ts in self.realData[tt]:
                l_isodate.add(ts)
                for area in self.realData[tt][ts]:
                    l_area.add(area)
        for tt in self.hPred:
            l_tt.add(tt)
            for ts in self.hPred[tt]:
                l_isodate.add(ts)
                for area in self.hPred[tt][ts]:
                    l_area.add(area)
        #
        self.l_isodate = list(l_isodate)
        self.l_area = list(l_area)
        self.l_tt = list(l_tt)
        self.log.info('Parsed %d transports, %d isodates, %d areas' %
                      (len(l_tt), len(l_isodate), len(l_area)))

    def get_stats(self):
        # compute lists of keys
        self.build_key_lists()
        self.fill_empty_values()
        # convert into DataFrame
        self.df = self.convert_data_to_df()
        # convert isodate into isodaydate
        self.df['isodaydate'] = self.df['isodate'].dt.date
        # evaluation metrics (by tt)
        print('Evaluation:')
        print('\n- overall AUC: %f' % eval_auc(self.df))
        for ft in ['transport', 'isodaydate']:
            print(self.evaluate_by_type(ft, 'auc'))
        print('\n- overall RMSE: %f' % eval_rmse(self.df))
        for ft in ['transport', 'isodaydate']:
            print(self.evaluate_by_type(ft, 'rmse'))
        # general stats
        # total demand
        # demand per areas
        # demand per tt

    def fill_empty_values(self):
        """
        parse both real demand and prediction and fill empty values
        """
        for tt in self.l_tt:
            if tt not in self.realData:
                self.realData[tt] = {}
            if tt not in self.hPred:
                self.hPred[tt] = {}
            #
            for ts in self.l_isodate:
                if ts not in self.realData[tt]:
                    self.realData[tt][ts] = {}
                if ts not in self.hPred[tt]:
                    self.hPred[tt][ts] = {}
                #
                for area in self.l_area:
                    if area not in self.realData[tt][ts]:
                        self.realData[tt][ts][area] = 0
                    if area not in self.hPred[tt][ts]:
                        self.hPred[tt][ts][area] = 0.0

    def convert_data_to_df(self):
        """
        convert prediction data and real demand into a single DataFrame
        """
        df = pd.DataFrame()
        for tt in self.l_tt:
            for ts in self.l_isodate:
                for area in self.l_area:
                    df = df.append({
                        'transport': tt,
                        'isodate': ts,
                        'area': area,
                        'real': self.realData[tt][ts][area],
                        'pred': self.hPred[tt][ts][area]
                    }, ignore_index=True)
        #
        df['isodate'] = pd.to_datetime(df['isodate'])
        df['transport'] = df['transport'].astype('category')
        # convert prediction into a binary array
        MIN = self.cfg['general']['min_pred_demand']
        df['pred_area'] = [1 if p > MIN else 0 for p in df['pred']]
        df['real_area'] = [1 if r > 0 else 0 for r in df['real']]
        #
        return(df)

    def evaluate_by_type(self, feat_type, metric):
        if metric == 'auc':
            return self.df.groupby([feat_type]).apply(eval_auc)
        elif metric == 'rmse':
            return self.df.groupby([feat_type]).apply(eval_rmse)


if __name__ == "__main__":
    transports = ['all', 'walk', 'bike', 'motorbike', 'car', 'van']
    DEVEL = True
    STARTAT = '2015-12-01T00:00:00+00:00'
    ENDAT = '2015-12-21T00:00:00+00:00'
    #
    conf_path = 'conf/standalone_predictor.yml'
    # Build and Update multiple dataframes
    print('Starting MultiDFBuilder')
    EVA = Evaluator(transports, conf_path, STARTAT, ENDAT)
    #
    EVA.get_stats()
