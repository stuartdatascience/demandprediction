# -*- coding: utf-8 -*-
# !/bin/env python

import pandas as pd

from os import listdir, remove
from os.path import isfile, join
from sklearn.externals import joblib

__author__ = "trevi"
__creation__ = "November 14, 2015"
__source__ = "https://github.com/StuartApp/DemandPrediction"
__description__ = "This class deal with requests from and to APIs."


def get_latest_file(l_filenames):
    """
    parse the list of filenames and return the one with the most updated
    date in the name
    """
    if len(l_filenames) > 0:
        l_filenames.sort()
        return l_filenames[-1]
    else:
        return ''


class FeatureVectorsCatcher:
    """
    Class that loads and dumps various feature vector(s).
    """

    def __init__(self, conf=None):
        self.s2level = None
        self.city = None
        self.max_files = 3

    def dump_df_vectors(self, df_vectors, all_vect_path, log):
        df_vectors.to_csv(all_vect_path, index=False)
        # find the folder
        fname = all_vect_path.split('/')[-1]
        fFP = all_vect_path[:-len(fname)]
        # remove older feature vectors
        filenames = [f for f in listdir(fFP) if isfile(join(fFP, f)) and
                     f.endswith('all_feature_vectors.csv')]
        # remove files if too many
        if len(filenames) >= self.max_files:
            filenames.sort()
            self.remove_old_data(
                fFP, filenames[:len(filenames)-self.max_files], log)

    def dump_all_feature_vectors(self, feat_vectors, all_vect_path):
        import cPickle as pickle
        pickle.dump(feat_vectors, open(all_vect_path, "wb"))

    def dump_current_features_vector(self, feat_vector, vect_path):
        import cPickle as pickle
        pickle.dump(feat_vector, open(vect_path, "wb"))

    def load_df_features(self, fFP, fn, log, tt='all'):
        filenames = [f for f in listdir(fFP) if isfile(join(fFP, f)) and
                     f.endswith(fn)]
        # remove files if too many
        if len(filenames) > self.max_files:
            filenames.sort()
            self.remove_old_data(
                fFP, filenames[:len(filenames)-self.max_files], log, tt)
        last_file = get_latest_file(filenames)
        if last_file == '':
            log.info('[%s] No DF Features Found' % tt)
            return {}
        log.info('[%s] DF Features Selected: %s' % (tt, last_file))
        return pd.read_csv(join(fFP, last_file))

    def load_current_df_feature(self, fFP, fn, log, tt='all'):
        filenames = [f for f in listdir(fFP) if isfile(join(fFP, f)) and
                     f.endswith(fn)]
        # remove files if too many
        if len(filenames) >= self.max_files:
            filenames.sort()
            self.remove_old_data(
                fFP, filenames[:len(filenames)-self.max_files], log, tt)
        # print info
        last_file = get_latest_file(filenames)
        log.info('[%s] DF Feature File Selected: %s (%d found)' %
                 (tt, last_file, len(filenames)))
        return pd.read_csv(join(fFP, last_file))

    def remove_old_data(self, fFP, filenames, log, tt):
        for f in filenames:
            log.info('-- [%s] Removing file %s' % (tt, f))
            remove(join(fFP, f))


class MLModelSerializer:
    """
    Class that loads and dumps the ML model.
    """

    def __init__(self, conf=None):
        self.s2level = None
        self.city = None
        self.max_files = 3

    def load_ml_model(self, mFP, fn, log, tt='all'):
        """
        get all the filename from the model folder, and load the model
        with the latest date in the filename
        """
        filenames = [f for f in listdir(mFP) if isfile(join(mFP, f)) and
                     f.endswith(fn)]
        # get the latest one
        model_name = get_latest_file(filenames)
        # remove files if too many
        if len(filenames) >= self.max_files:
            filenames.sort()
            self.remove_old_data(
                mFP, filenames[:len(filenames)-self.max_files], log, tt)
        log.info('[%s] Model selected: %s (%d found)' %
                 (tt, model_name, len(filenames)))
        return joblib.load(join(mFP, model_name))

    def dump_ml_model(self, model, model_path):
        """
        dump ML model with given date
        """
        joblib.dump(model, model_path, compress=9)

    def remove_old_data(self, fFP, filenames, log, tt):
        for f in filenames:
            log.info('-- [%s] Removing file %s' % (tt, f))
            remove(join(fFP, f))
