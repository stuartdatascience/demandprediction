# -*- coding: utf-8 -*-
# !/bin/env python

import simplejson as json
import dateutil.parser
import requests
import sys

from time import time
from datetime import timedelta

__author__ = "trevi"
__creation__ = "November 14, 2015"
__source__ = "https://github.com/StuartApp/DemandPrediction"
__description__ = "This class deal with requests from and to APIs."


class AllTTDataAPICatcher:

    """
    Class that deals with the API calls to retrieve data for all the
    transport type in order to reduce the number of calls.
    """

    def __init__(self, log, area, city, conf, s2level=None):
        self.name = __name__
        self.log = log
        self.area = area
        self.city = city
        self.s2level = s2level
        self.conf = conf
        self.transports = ['walk', 'bike', 'motorbike', 'car', 'van']
        self.d_geoname = {}

    def load_new_data_for_all_tt(self, from_date, to_date):
        """
        Call the API class to retrieve the data of all the time-slot that are
        not included in the vectors (previously computed).
        @return the new data obtained
        """
        cur_date = from_date
        final_date = to_date + timedelta(days=1)
        d_newdata = {
            'walk': {},
            'bike': {},
            'motorbike': {},
            'car': {},
            'van': {}
            }
        st = time()
        no_calls = 0
        # make API request for each timeslot
        OFFSET = 10
        while cur_date <= (final_date):
            no_calls += 1
            next_isod_obj = min(cur_date + timedelta(days=OFFSET), final_date)
            cur_isod_str = cur_date.strftime('%Y-%m-%dT%H:%M:%S%z')
            next_isod_str = next_isod_obj.strftime('%Y-%m-%dT%H:%M:%S%z')
            self.log.info('** API: Retrieving data from %s to %s' %
                          (cur_isod_str, next_isod_str))
            # API Call
            d_new = self.get_all_data_from_api(
                from_date=cur_isod_str,
                to_date=next_isod_str,
                api_url=self.conf
                )
            # check API answer
            if type(d_new) is not dict:
                self.log.info(
                    '** Error retrieving time-slot (%s,%s) from API' %
                    (cur_isod_str, next_isod_str))
                self.log.info(d_new)
                sys.exit(1)
            else:
                for tt in d_new:
                    d_newdata[tt].update(d_new[tt])
            cur_date = next_isod_obj
            # break if the loop is over
            if final_date == next_isod_obj:
                break
        # print stats
        if no_calls > 0:
            self.log.info('** %d APIs calls in ~%.2f seconds' %
                          (no_calls, time()-st))
        else:
            self.log.info('** NO APIs calls done - the dataframe is updated')
        return d_newdata

    def get_all_data_from_api(self, from_date, to_date, api_url):
        """
        https://github.com/StuartApp/demand-prediction-api
        """
        payload = {
            'city': self.city,
            'from_date': from_date,
            'to_date': to_date
            }
        # add s2level if cell required
        if self.area == 'cell':
            payload['s2level'] = self.s2level
        st = time()
        r = requests.get(api_url, params=payload)
        self.log.info('** Return result ~%.2f seconds' % (time()-st))
        # check error
        if r.status_code != 200:
            return r.status_code
        try:
            jdata = json.loads(r.text)
            return self.parse_new_data_tt(jdata['new_jobs'])
        except:
            return "** Unexpected error:", sys.exc_info()[0]

    def parse_new_data_tt(self, jdata_new):
        """
        parse last data received from API into dictionary
        """
        d_data = {}
        self.log.info('** Parsing API results')
        for job in jdata_new:
            isodate_obj = dateutil.parser.parse(job['created_at'])
            dayS = str(isodate_obj.date())
            shift = self.get_day_shift(isodate_obj)
            #
            if self.area == 'cell':
                area_id = int(job['origin_s2_cell'])
            elif self.area == 'neighborhood':
                area_id = int(job['neighborhood_id'])
                if area_id not in self.d_geoname:
                    self.d_geoname[area_id] = job['origin_neighborhood']
            else:
                self.log.error('Area type (%s) not recognized' % self.area_id)
                sys.exit()
            #
            # update each transport type that is requested
            req_transports = [str(t) for t in job['requested_transport_types']]
            for tt in self.transports:
                if tt not in d_data:
                    d_data[tt] = {}
                if dayS not in d_data[tt]:
                    d_data[tt][dayS] = {
                        'morning': {},
                        'noon': {},
                        'afternoon': {},
                        'evening': {},
                        'other': {}
                    }
                if area_id not in d_data[tt][dayS][shift]:
                    d_data[tt][dayS][shift][area_id] = {}
                    d_data[tt][dayS][shift][area_id]['demand'] = 0
                    d_data[tt][dayS][shift][area_id]['l_client'] = []
                    d_data[tt][dayS][shift][area_id]['isodate'] = isodate_obj
                # update value if in req_transports
                if tt in req_transports:
                    d_data[tt][dayS][shift][area_id]['demand'] += 1
                    d_data[tt][dayS][shift][area_id]['l_client']\
                        .append(job['client_id'])
        #
        return d_data

    def get_day_shift(self, idateO):
        if '08:00' <= str(idateO.time()) < '11:30':
            return 'morning'
        elif '11:30' <= str(idateO.time()) < '15:30':
            return 'noon'
        elif '15:30' <= str(idateO.time()) < '19:00':
            return 'afternoon'
        elif '19:00' <= str(idateO.time()) <= '23:00':
            return 'evening'
        else:
            return 'other'

    def load_predictions_for_all_tt(self, rec_isod_obj, now_isod_obj):
        """
        Call the API class to retrieve the prediction by hours for each
        time slot.
        @return the new data obtained
        """
        cur_isod_obj = rec_isod_obj + timedelta(hours=1)
        d_newpred = {
            'all': {},
            'walk': {},
            'bike': {},
            'motorbike': {},
            'car': {},
            'van': {}
            }
        st = time()
        no_calls = 0
        # make API request for each timeslot
        OFFSET = 24*7
        while cur_isod_obj <= (now_isod_obj - timedelta(hours=1)):
            no_calls += 1
            cur_isod_str = cur_isod_obj.strftime('%Y-%m-%dT%H:%M:%S%z')
            next_isod_obj = min(
                cur_isod_obj + timedelta(hours=OFFSET), now_isod_obj)
            # break if the loop is over
            if cur_isod_obj == next_isod_obj:
                break
            next_isod_str = next_isod_obj.strftime('%Y-%m-%dT%H:%M:%S%z')
            self.log.info('** API: Retrieving data from %s to %s' %
                          (cur_isod_str, next_isod_str))
            # API Call
            d_new = self.get_pred_from_api(
                from_date=cur_isod_str,
                to_date=next_isod_str,
                api_url=self.conf
                )
            # check API answer
            if type(d_new) is not dict:
                self.log.info(
                    '** Error retrieving time-slot (%s,%s) from API' %
                    (cur_isod_str, next_isod_str))
                self.log.info(d_new)
                sys.exit(1)
            else:
                for tt in d_new:
                    kid = 'walk' if tt == 'walker' else tt
                    d_newpred[kid].update(d_new[tt])
            cur_isod_obj = next_isod_obj
        # print stats
        if no_calls > 0:
            self.log.info('** %d APIs calls in ~%.2f seconds' %
                          (no_calls, time()-st))
        else:
            self.log.info('** NO APIs calls done - the dataframe is updated')
        return d_newpred

    def get_pred_from_api(self, from_date, to_date, api_url):
        """
        https://github.com/StuartApp/demand-prediction-api
        """
        payload = {
            'city': self.city,
            'from_date': from_date,
            'to_date': to_date
            }
        # add s2level if cell required
        if self.area == 'cell':
            payload['s2level'] = self.s2level
        st = time()
        r = requests.get(api_url, params=payload)
        self.log.info('** Return result ~%.2f seconds' % (time()-st))
        # check error
        if r.status_code != 200:
            return r.status_code
        try:
            jdata = json.loads(r.text)
            return self.parse_new_pred_tt(jdata['predictions'])
        except:
            return "** Unexpected error:", sys.exc_info()[0]

    def parse_new_pred_tt(self, jdata_new):
        """
        parse last data received from API into dictionary
        """
        d_data = {}
        self.log.info('** Parsing API results')
        for ts in jdata_new:
            if 'start_at' in ts:
                isodate_obj = dateutil.parser.parse(ts['start_at'])
            else:
                isodate_obj = dateutil.parser.parse(ts['startAt'])
            isodate_str = isodate_obj.replace(minute=00, second=00).isoformat()
            #
            for pred in ts['prediction']:
                area = int(pred['aid'])
                tot_pred = pred['total_predicted_demand']
                # update overall predicted demand
                if 'all' not in d_data:
                    d_data['all'] = {}
                if isodate_str not in d_data['all']:
                    d_data['all'][isodate_str] = {}
                if area not in d_data['all'][isodate_str]:
                    d_data['all'][isodate_str][area] = tot_pred
                # update each transport type that is requested
                for tt in pred['driver_type']:
                    if tt not in d_data:
                        d_data[tt] = {}
                    if isodate_str not in d_data[tt]:
                        d_data[tt][isodate_str] = {}
                    if area not in d_data[tt][isodate_str]:
                        d_data[tt][isodate_str][area] = pred['driver_type'][tt]
        #
        return d_data
