# -*- coding: utf-8 -*-
# !/bin/env python

import dateutil
from datetime import timedelta
from workalendar.europe import France


class FeaturesBuilder:

    """
    Then compute the feature vectors
    """

    def __init__(self, city, d_rawdata, l_area, l_shift):
        self.d_feat = None
        self.city = city
        self.d_new = d_rawdata
        self.df_vectors = None
        self.l_area = l_area
        self.l_shift = l_shift
        self.l_feat = ['demand', 'clients', 'unique_clients', 'holidays',
                       'morning', 'noon', 'afternoon', 'evening', 'other']

    def initialize_features(self, shift):
        d_feat = {}
        for f in self.l_feat:
            d_feat[f] = 0
        d_feat[shift] = 1
        return d_feat

    def fill_cell_vector(self, daydateS, d_data):
        self.d_data = d_data
        self.dayS = daydateS
        self.dayO = dateutil.parser.parse(daydateS)
        # update cells
        for s in self.l_shift:
            if s not in d_data[self.dayS]:
                d_data[self.dayS][s] = {}
            for a in self.l_area:
                # initialize basic features
                d_data[self.dayS][s][a] = self.initialize_features(s)
                # check if the area has been stored
                if self.dayS in self.d_new and a in self.d_new[self.dayS][s]:
                    # check if shifts have been stored
                    l_client = self.d_new[self.dayS][s][a]['l_client']
                    d_data[self.dayS][s][a]['clients'] = len(l_client)
                    d_data[self.dayS][s][a]['unique_clients'] = \
                        len(set(l_client))
                    d_data[self.dayS][s][a]['demand'] = \
                        self.d_new[self.dayS][s][a]['demand']
                # add holidays information
                if self.city == 'Paris':
                    self.dict_addHolidayFlag(a, s, country='France')
                # add cell temporal features
                self.dict_addHistoricalDemand(a, s, 'week', 1)
                self.dict_addHistoricalDemand(a, s, 'week', 2)
                self.dict_addHistoricalDemand(a, s, 'week', 3)
                # add cell temporal clients stats
                self.dict_addHistoricalClients(a, s, 'week', 1)
                self.dict_addHistoricalClients(a, s, 'week', 2)
                self.dict_addHistoricalClients(a, s, 'week', 3)
                # add timeslot features
                # self.dict_addGeocodedMonths(a, s, datetime)
                # self.dict_addGeocodedWeeks(a, s, datetime)
                self.dict_addGeocodedDays(a, s)
                # self.dict_addGeocodedHours(a, s, datetime)
        return self.d_data

    # Compute Timeslot Features

    def dict_addGeocodedMonths(self, a, s):
        for i in range(1, 13):
            key = 'm%d' % i
            if key not in self.l_feat:
                self.l_feat.append(key)
            if key not in self.d_data[self.dayS][s][a]:
                self.d_data[self.dayS][s][a][key] = 0
        self.d_data[self.dayS][s][a]['m%d' % self.dayO.month] = 1

    def dict_addGeocodedWeeks(self, a, s):
        for i in range(1, 53):
            key = 'w%d' % i
            if key not in self.l_feat:
                self.l_feat.append(key)
            if key not in self.d_data[self.dayS][s][a]:
                self.d_data[self.dayS][s][a][key] = 0
        #
        week_idx = self.dayO.isocalendar()[1]
        self.d_data[self.dayS][s][a]['w%d' % week_idx] = 1

    def dict_addGeocodedDays(self, a, s):
        l_feat = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
        for key in l_feat:
            if key not in self.l_feat:
                self.l_feat.append(key)
            if key not in self.d_data[self.dayS][s][a]:
                self.d_data[self.dayS][s][a][key] = 0
        #
        dayidx = self.dayO.weekday()
        self.d_data[self.dayS][s][a][l_feat[dayidx]] = 1

    def dict_addGeocodedHours(self, a, s):
        for i in range(1, 25):
            key = 'h%d' % i
            if key not in self.l_feat:
                self.l_feat.append(key)
            if key not in self.d_data[self.dayS][s][a]:
                self.d_data[self.dayS][s][a][key] = 0
        #
        self.d_data[self.dayS][s][a]['%dh' % self.dayO.hour] = 1

    # Compute Cell Features

    def dict_addHistoricalDemand(self, a, s, timeslot, offset):
        # get previous time slot
        if timeslot == 'day':
            prevDateStr = str((self.dayO - timedelta(days=offset)).date())
        elif timeslot == 'week':
            prevDateStr = str((self.dayO - timedelta(weeks=offset)).date())
        #
        feat_name = 'demand_%s_%d' % (timeslot, offset)
        if feat_name not in self.l_feat:
            self.l_feat.append(feat_name)
        if feat_name not in self.d_data[self.dayS][s][a]:
            self.d_data[self.dayS][s][a][feat_name] = 0
        #
        if prevDateStr in self.d_data:
            if a in self.d_data[prevDateStr][s]:
                self.d_data[self.dayS][s][a][feat_name] = \
                    self.d_data[prevDateStr][s][a]['demand']

    def dict_addHistoricalClients(self, a, s, timeslot, offset):
        # get previous time slot
        if timeslot == 'day':
            prevDateStr = str((self.dayO - timedelta(days=offset)).date())
        elif timeslot == 'week':
            prevDateStr = str((self.dayO - timedelta(weeks=offset)).date())
        #
        feat_client = 'clients_%s_%d' % (timeslot, offset)
        if feat_client not in self.l_feat:
            self.l_feat.append(feat_client)
        if feat_client not in self.d_data[self.dayS][s][a]:
            self.d_data[self.dayS][s][a][feat_client] = 0
        feat_unique = 'unique_clients_%s_%d' % (timeslot, offset)
        if feat_unique not in self.l_feat:
            self.l_feat.append(feat_unique)
        if feat_unique not in self.d_data[self.dayS][s][a]:
            self.d_data[self.dayS][s][a][feat_unique] = 0
        #
        if prevDateStr in self.d_data:
            if a in self.d_data[prevDateStr][s]:
                self.d_data[self.dayS][s][a][feat_client] = \
                    self.d_data[prevDateStr][s][a]['clients']
                self.d_data[self.dayS][s][a][feat_unique] = \
                    self.d_data[prevDateStr][s][a]['unique_clients']

    def dict_addHolidayFlag(self, a, s, country='France'):
        if country == 'France':
            cal = France()
        if not cal.is_working_day(self.dayO):
            self.d_data[self.dayS][s][a]['holidays'] = 1
