#!/bin/env python
# This is a helper class for running paramater grid search across different
# classification or regression models.
# http://codiply.com/blog/hyperparameter-grid-search-across-multiple-models-in-scikit-learn
# https://github.com/codiply/blog-ipython-notebooks/blob/master/scikit-learn-estimator-selection-helper.ipynb

import math
import itertools
import numpy as np
import pandas as pd

from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor

from sklearn.metrics import f1_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import mean_squared_error


class mtRegressionEstimatorSelection:

    def __init__(self, params):
        self.params = params
        self.keys = params.keys()
        self.estimators = {}
        for k in self.keys:
            self.estimators[k] = []

    def fit(self, X, y, n_jobs=1, verbose=1, refit=False):
        for key in self.keys:
            # print("Comparing Evaluators for %s." % key)
            for pcomb in self.get_param_combinations(self.params[key]):
                if key == 'RandomForestRegressor':
                    est = RandomForestRegressor().set_params(**pcomb)
                elif key == 'ExtraTreesRegressor':
                    est = ExtraTreesRegressor().set_params(**pcomb)
                elif key == 'GradientBoostingRegressor':
                    est = GradientBoostingRegressor().set_params(**pcomb)
                est.fit(X, y)
                self.estimators[key].append((est, pcomb))
        #

    def get_param_combinations(self, params):
        param_comb = []
        param_names = list(params.keys())
        param_values = list(params.values())
        for comb in itertools.chain(itertools.product(*param_values)):
            param_comb.append(comb)
        # create dictionary
        l_diz = list()
        for comb in param_comb:
            diz = dict()
            for i in range(0, len(param_names)):
                diz.update({param_names[i]: comb[i]})
            l_diz.append(diz)
        return l_diz

    def rmsle(self, y, p):
        values = []
        for i in range(y.size):
            values.append((math.log(p[i]+1) - math.log(y[i]+1)) ** 2)
        return math.sqrt(float(sum(values)) / len(values))

    def compute_metrics(self, y_true, preds, min_val):
        c_preds = np.array([1 if i > min_val else 0 for i in preds])
        c_ytrue = np.array([1 if i > min_val else 0 for i in y_true])
        res = {}
        try:
            res['AUC'] = roc_auc_score(c_ytrue, c_preds)
        except:
            res['AUC'] = None
        try:
            res['F1'] = f1_score(c_ytrue, c_preds)
        except:
            res['F1'] = None
        res['Recall'] = recall_score(c_ytrue, c_preds)
        res['Precision'] = precision_score(c_ytrue, c_preds)
        res['RMSE'] = mean_squared_error(y_true, preds)**0.5
        res['RMSLE'] = self.rmsle(y_true, preds)
        return res

    def score_summary(self, Xval, yval, sort_by='AUC', min_val=1):

        def row(key, preds, params):
            preds = np.array([0.0 if p < min_val else p for p in preds])
            res = self.compute_metrics(yval, preds, min_val=min_val)
            return pd.Series(dict(
              [('Estimator', key)] + list(params.items()) + list(res.items())
            ))

        rows = [row(k, gsc.predict(Xval), pcomb)
                for k in self.keys
                for gsc, pcomb in self.estimators[k]]

        df = pd.concat(rows, axis=1).T.sort_values(
            by=[sort_by], ascending=False)

        columns = ['Estimator',
                   'AUC', 'F1', 'Recall', 'Precision', 'RMSE', 'RMSLE']
        columns = columns + [c for c in df.columns if c not in columns]

        return df[columns]
