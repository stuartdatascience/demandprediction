# -*- coding: utf-8 -*-
# !/bin/env python

from time import time
from random import choice
from lib.apiDataCatcher import *
from lib.featureBuilder import *
from lib.localDataCatcher import *
from datetime import datetime, timedelta

from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.ensemble import GradientBoostingRegressor

from lib.EstimatorSelectionHelper import *

import logging.handlers
import dateutil.parser
import pandas as pd
import numpy as np
import logging
import yaml

__author__ = "trevi"
__creation__ = "November 10, 2015"
__source__ = "https://github.com/StuartApp/DemandPrediction"
__description__ = "Build the feature vectors for the new time slot (~1h slots)\
that have not been computed yet."


class DFBuilder:

    """
    Load the historical vectors (if any) and receive from the API the
    last data grouped by time slot (~1h) available in the DB.
    Then compute the remaining feature vectors.
    """

    def __init__(self, conf_path):
        # detect configuration
        try:
            with open(conf_path) as conf_f:
                self.cfg = yaml.load(conf_f)
        except:
            raise IOError('Configuration File Not Found')
        #
        self.log = self.setup_logger()
        # load newdata
        self.d_geoname = None
        self.d_rawdata = self.retrive_historical_data()
        self.l_area, self.l_shift = self.get_lists()
        self.df_vectors = {}

    def setup_logger(self):
        """
        configure logging system
        """
        self.cur_day = datetime.now().replace(minute=00, second=00)\
            .strftime('%Y-%m-%d')
        # set log filename
        LOG_FILENAME = self.cfg['general']['log_path']\
            .replace('DATE', self.cur_day)\
            .replace('AREA', self.cfg['general']['area_type'])
        # set up logging to file - see previous section for more details
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(funcName)-30s %(message)s',
            datefmt='%m-%d %H:%M',
            filename=LOG_FILENAME,
            filemode='w')
        # Set up a specific logger
        logger = logging.getLogger()
        logger.info('\n\n\n')
        return logger

    def retrive_historical_data(self):
        """
        makes common queries for all transport type combined in order
        to save time and improve performances
        """
        hist_weeks = self.cfg['builder']['past_weeks']
        area = self.cfg['general']['area_type']
        api_url = self.cfg['builder']['api'][area]['get_feat_data']
        self.log.info('** Getting dates from all transport type')
        # get time range for API calls
        today = datetime.now().date()
        to_sunday = today - timedelta(days=today.weekday()+1)
        from_monday = to_sunday - timedelta(weeks=hist_weeks, days=-1)
        # create TTAPI
        ttapi = AllTTDataAPICatcher(
            log=self.log,
            area=area,
            city=self.cfg['general']['city'],
            s2level=self.s2level if area == 'cell' else None,
            conf=api_url)
        # call TT API with these time range
        newTTData = ttapi.load_new_data_for_all_tt(from_monday, to_sunday)
        self.d_geoname = ttapi.d_geoname
        return newTTData

    def get_lists(self):
        """
        @return: set() of area ids and shifts
        """
        s_area = set()
        s_shift = set()
        for tt in self.d_rawdata:
            for d in self.d_rawdata[tt]:
                for s in self.d_rawdata[tt][d]:
                    s_shift.add(s)
                    s_area = s_area.union(set(self.d_rawdata[tt][d][s].keys()))
        return list(s_area), list(s_shift)

    def build_feature_vectors(self):
        st = time()
        self.log.info('Building dataframe of features from scratch')
        self.create_dataframe_with_rawdata()
        self.log.info('Completed in ~%.2f seconds' % (time()-st))

    def create_dataframe_with_rawdata(self):
        """
        create new dataframe after building all the features with a dictionary
        in order to improve the speed, and then converting it into a dataframe
        """
        for tt in self.cfg['general']['transport']:
            # sort date to start from the older one
            sorted_dates = self.add_prediction_dates(self.d_rawdata[tt].keys())
            sorted_dates.sort()
            #
            d_data = {}
            fb = FeaturesBuilder(
                city=self.cfg['general']['city'],
                d_rawdata=self.d_rawdata[tt],
                l_area=self.l_area,
                l_shift=self.l_shift)
            # parse each time slot in the new_data
            for dayS in sorted_dates:
                # create entry in the dictionary
                if dayS not in d_data:
                    d_data[dayS] = {}
                # compute features for each cell
                d_data = fb.fill_cell_vector(dayS, d_data)
                self.l_feat = fb.l_feat
            # convert dictionary into dataframe
            self.df_vectors[tt] = self.convert_dict_to_df(d_data, tt)
        #

    def add_prediction_dates(self, l_dates):
        """
        add dates and shifts of the week that needs to be predicted
        """
        l_dates = list(l_dates)
        last_day = dateutil.parser.parse(max(l_dates))
        final_day = last_day + timedelta(weeks=1)
        while last_day < final_day:
            last_day += timedelta(days=1)
            l_dates.append(str(last_day.date()))
        return l_dates

    def convert_dict_to_df(self, d_data, tt):
        new_dict = {}
        self.l_feat = ['date', 'transport', 'time_shift',
                       'geo', 'geo_name'] + self.l_feat
        for dayS in d_data:
            for s in d_data[dayS]:
                for a in d_data[dayS][s]:
                    idx = len(new_dict)
                    new_dict[idx] = {}
                    new_dict[idx]['date'] = dayS
                    new_dict[idx]['time_shift'] = s
                    new_dict[idx]['geo'] = a
                    new_dict[idx]['geo_name'] = self.d_geoname[a]
                    new_dict[idx]['transport'] = tt
                    for feat in d_data[dayS][s][a]:
                        new_dict[idx][feat] = d_data[dayS][s][a][feat]
        # convert into dataframe
        self.log.info('Convert plain dictionary into dataframe')
        df = pd.DataFrame.from_dict(new_dict, orient='index')
        # print df[self.labels].head()
        return df

    def store_all_feature_vectors(self):
        ldc = FeatureVectorsCatcher()
        # Change output filename based on current date
        idateO = datetime.now().date()
        idateS = str(idateO)
        # construct output name
        for tt in self.df_vectors:
            out_vect_path = self.cfg['general']['path']['all_vectors']
            out_vect_path = out_vect_path\
                .replace('DATE', idateS)\
                .replace('AREA', self.cfg['general']['area_type'])\
                .replace('TYPE', tt)
            self.log.info('++ [%s] Dumping DF features into CSV file: %s' %
                          (tt, out_vect_path))
            ldc.dump_df_vectors(self.df_vectors[tt][self.l_feat],
                                out_vect_path, self.log)


class ModelTrainer:

    """
    """

    def __init__(self, conf_path):
        # detect configuration
        try:
            self.cfg = yaml.load(open(conf_path))
        except:
            raise IOError('Configuration File Not Found')
        #
        self.log = self.setup_logger()
        self.model = {}
        self.df = {}

    def setup_logger(self):
        """
        configure logging system
        """
        self.cur_day = str(datetime.now().date())
        # set log filename
        LOG_FILENAME = self.cfg['general']['log_path']\
            .replace('DATE', self.cur_day)\
            .replace('AREA', self.cfg['general']['area_type'])
        # set up logging to file - see previous section for more details
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(funcName)-30s %(message)s',
            datefmt='%m-%d %H:%M',
            filename=LOG_FILENAME,
            filemode='w')
        # Set up a specific logger
        logger = logging.getLogger()
        logger.info('\n\n\n')
        return logger

    def load_df_features(self):
        for tt in self.cfg['general']['transport']:
            ldc = FeatureVectorsCatcher()
            # define path
            vect_path = self.cfg['general']['path']['all_vectors']
            vect_path = vect_path\
                .replace('AREA', self.cfg['general']['area_type'])\
                .replace('TYPE', tt)
            fFP, fn = vect_path.split('DATE')
            self.log.info('[%s] Loading DF Features from %s' % (tt, fFP))
            self.df[tt] = ldc.load_df_features(fFP, fn, self.log, tt)
            # data cleansing
            self.df[tt]['date'] = pd.to_datetime(self.df[tt]['date'])
        #

    def data_split(self):
        """
        select last week of data as test set
        """
        to_sunday = max(self.df['bike'].date).to_datetime().date()
        from_monday = to_sunday - timedelta(weeks=1, days=-1)
        wn = from_monday.isocalendar()[1]
        print('Forecasting Week: %d' % wn)
        self.dftest = {}
        self.dftrain = {}
        for tt in self.cfg['general']['transport']:
            self.dftest[tt] = self.df[tt][self.df[tt].date >= from_monday]
            self.dftrain[tt] = self.df[tt][self.df[tt].date < from_monday]
        # print info about datasets
        rnd_tt = choice(self.cfg['general']['transport'])
        print('Train set time range: (%s -> %s)' %
              (min(self.dftrain[rnd_tt].date).to_datetime().date(),
               max(self.dftrain[rnd_tt].date).to_datetime().date()))
        print('Test set time range: (%s -> %s)' %
              (min(self.dftest[rnd_tt].date).to_datetime().date(),
               max(self.dftest[rnd_tt].date).to_datetime().date()))
        #

    def train_models(self, store_models=False):
        """
        Set up the model with the configuration parameters, train the model
        with the given train set, and then return the trained model
        """
        # split the data into train and validation set
        self.data_split()
        #
        for tt in self.cfg['general']['transport']:
            self.log.info('[%s] Dataframe pre-processing' % tt)
            # define sample weight for each observation
            mtype = self.cfg['general']['model_type'][tt]
            sample_w = -1
            if 'sample_w' in self.cfg[mtype][tt]:
                sample_w = float(self.cfg[mtype][tt]['sample_w'])
            # select features (discarding demand and date)
            self.features = list(self.dftrain[tt].columns)
            # setting up the model
            st = time()
            self.model[tt] = self.define_model_params(mtype, tt)
            # get the label
            self.y, _ = pd.factorize(self.dftrain[tt]['demand'])
            if sample_w > 0:
                sw = np.array([1.0 if d == 0 else sample_w for d in self.y])
            # remove fields from the features
            self.features.remove('geo_name')
            self.features.remove('transport')
            self.features.remove('clients')
            self.features.remove('unique_clients')
            self.features.remove('date')
            self.features.remove('time_shift')
            self.features.remove('demand')
            #
            self.log.info('[%s] Considering %d features' %
                          (tt, len(self.features)))
            # train the model
            self.log.info('[%s] Training the model' % tt)
            if sample_w > 0:
                self.log.info('[%s] Using sample_weight set to %f' %
                              (tt, sample_w))
                self.model[tt].fit(
                    self.dftrain[tt][self.features],
                    self.y,
                    sample_weight=sw)
            else:
                self.model[tt].fit(self.dftrain[tt][self.features], self.y)
            self.log.info('[%s] Model trained in ~%.2f seconds' %
                          (tt, (time()-st)))
        # save model if requested
        if store_models:
            self.save_models()

    def define_model_params(self, mtype, tt):
        model_config = self.cfg[mtype][tt]
        if 'RandomForest' in mtype:
            model = RandomForestRegressor(
                n_jobs=model_config['ncpus'],
                n_estimators=model_config['ntree'],
                max_depth=model_config['max_depth'],
                max_features=model_config['max_features'],
                min_samples_split=model_config['min_samples_split'],
                min_samples_leaf=model_config['min_samples_leaf']
                )
        elif 'ExtraTree' in mtype:
            model = ExtraTreesRegressor(
                n_jobs=model_config['ncpus'],
                n_estimators=model_config['ntree'],
                max_depth=model_config['max_depth'],
                max_features=model_config['max_features'],
                min_samples_split=model_config['min_samples_split'],
                min_samples_leaf=model_config['min_samples_leaf']
                )
        elif 'GradientBoosting' in mtype:
            model = GradientBoostingRegressor(
                learning_rate=model_config['learning_rate'],
                max_depth=model_config['max_depth'],
                max_features=model_config['max_features'],
                min_samples_leaf=model_config['min_samples_leaf'],
                n_estimators=model_config['n_estimators'],
                subsample=model_config['subsample']
                )
        else:
            self.log.error('Could not find any model type named: %s' % mtype)
            sys.exit()
        #
        return model

    def save_models(self):
        for tt in self.cfg['general']['transport']:
            ms = MLModelSerializer()
            # define model name
            mtype = self.cfg['general']['model_type'][tt]
            model_path = self.cfg[mtype]['model_path']
            model_path = model_path\
                .replace('DATE', self.cur_day)\
                .replace('AREA', self.cfg['general']['area_type'])\
                .replace('TRANSPORT', tt)
            # dump model
            st = time()
            self.log.info('++ [%s] Dumping model to %s' % (tt, model_path))
            ms.dump_ml_model(self.model[tt], model_path)
            self.log.info('++ [%s] Model dumped in ~%.2f seconds' %
                          (tt, (time()-st)))

    def make_forecasting(self, store_forecasting=False):
        l_df_pred = []
        min_pred = self.cfg['predictor']['min_pred_demand']
        for tt in self.cfg['general']['transport']:
            pred = self.model[tt].predict(self.dftest[tt][self.features])
            fltpred = np.array([0 if p < min_pred else round(p) for p in pred])
            cur_df_pred = pd.DataFrame({
                'date': self.dftest[tt].date,
                'geo': self.dftest[tt].geo,
                'neighborhood': self.dftest[tt].geo_name,
                'time_shift': self.dftest[tt].time_shift,
                'transport': tt,
                'prediction': fltpred
                })
            l_df_pred.append(cur_df_pred)
        #
        self.df_pred = pd.concat(l_df_pred)
        self.df_pred['prediction'] = self.df_pred['prediction'].astype(int)
        #
        columns = ['date', 'time_shift', 'neighborhood',
                   'transport', 'prediction']
        if store_forecasting:
            week_num = 'week%d' % datetime.now().isocalendar()[1]
            pred_out_file = self.cfg['predictor']['output_file']\
                .replace('DATE', self.cur_day)\
                .replace('WEEKNUMBER', week_num)
            self.log.info('++ ** Dumping predictions to %s' % pred_out_file)
            self.df_pred[columns].to_csv(
                pred_out_file,
                sep=',',
                encoding='utf-8',
                index=False
                )
            # save also xls file
            restable = self.df_pred[columns][self.df_pred.prediction > 0]\
                .groupby(['date', 'time_shift', 'transport', 'neighborhood'])\
                .sum()
            restable.to_excel(pred_out_file.replace('csv', 'xls'))
        else:
            print(self.df_pred.groupby('transport').head())


if __name__ == "__main__":
    # collect data and create dataframe
    # DFB = DFBuilder(conf_path='conf/weekly_shift_predictor.yml')
    # DFB.build_feature_vectors()
    # DFB.store_all_feature_vectors()
    #
    # build and train model
    MB = ModelTrainer(conf_path='conf/weekly_shift_predictor.yml')
    MB.load_df_features()
    MB.train_models(store_models=True)
    MB.make_forecasting(store_forecasting=True)
