
[![Code Climate](https://codeclimate.com/repos/564c8fdb6956801247007d00/badges/d3295dc93b605a251315/gpa.svg)](https://codeclimate.com/repos/564c8fdb6956801247007d00/feed)
[![Issue Count](https://codeclimate.com/repos/564c8fdb6956801247007d00/badges/d3295dc93b605a251315/issue_count.svg)](https://codeclimate.com/repos/564c8fdb6956801247007d00/feed)
[![Test Coverage](https://codeclimate.com/repos/564c8fdb6956801247007d00/badges/d3295dc93b605a251315/coverage.svg)](https://codeclimate.com/repos/564c8fdb6956801247007d00/coverage)

# Data Flow Diagram

![alt tag](https://github.com/StuartApp/DemandPrediction/blob/master/diagram/prediction_diagram.png)

# 1. Feature Vectors Builder

Run **every hour**:

- Save to the DB the previous hour vector (now with the real demand)
- Compute the vector of the current hour (without the real demand)

## Input

##### From the local machine

- Configuration file
- Feature vectors of the historical hours

##### API

Check out the [demand-prediction-api](https://github.com/StuartApp/demand-prediction-api)

## Output Feature collection

##### To the local machine

- Feature vectors updated with the last computed ones
- Feature vector of the current hour (without real demand)

##### API

Nothing.


---


# 2. Demand Predictor

Run **every hour**:

- Load the vector of the current hour computed by (1)
- Load the ML model trained by (3)
- Make the demand prediction for the current hour

## Input

##### From the local machine

- Configuration file
- Current vector computed by script (1)
- Model computed by script (3)

#### API

Nothing.

## Output

#### API

Check out the [demand-prediction-api](https://github.com/StuartApp/demand-prediction-api)

Note: We should also save the predictions into a log file (for long term retention).


---


# 3. ML Model Trainer

Run **every 24h after midnight**:

- Load the <N> last historical feature vectors
- Train the ML model

## Input

#### From the local machine

- Configuration file
- Feature vectors

#### API

Nothing.

## Output

#### To the local machine

- ML model

##### API

Nothing.
