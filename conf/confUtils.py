#!/bin/env python
# -*- coding: utf-8 -*-

# Set logging info
import logging
logging.basicConfig()
confLogger = logging.getLogger(__name__)
confLogger.setLevel(logging.INFO)
# Set configuration 
import ConfigParser
Config = ConfigParser.ConfigParser()

def ConfigSectionMap(conf_file, section):
	# load setting file
	Config.read(conf_file)
	# load dictionary
	confDict = {}
	options = Config.options(section)
	for option in options:
		try:
			confDict[option] = Config.get(section, option)
			if confDict[option] == -1:
				DebugPrint("skip: %s" % option)
		except:
			confLogger.error(' exception on %s!' % option)
			confDict[option] = None
	return confDict

def PrintSettings(conf):
	confLogger.info(' Loaded Settings:')
	for k in conf:
		confLogger.info(' %s: %s' %(k,conf[k]) )