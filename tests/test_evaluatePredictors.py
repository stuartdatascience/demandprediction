import sys
import unittest

sys.path.append('/Users/trevi/Dropbox/Projects/Stuart/DemandPrediction')

from evaluationPredictors import *

__author__ = 'trevi'


class TestEvaluatePredictors(unittest.TestCase):
    """
    python unittest: http://pythontesting.net/framework/unittest/unittest-introduction/
    """

    def test_binary(self):
        """ test the list conversion to a binary array """
        d_pred = {
            'g1': {'avg': 7},
            'g2': {'avg': 15},
            'g3': {'avg': 8}
        }
        d_real = {
            'g1': {'demand': 8},
            'g4': {'demand': 13},
            'g5': {'demand': 6}
        }
        # get unary arrays
        ev = EvaluatePredictors()
        # convert to vectors
        l_tot_geo = list(set(d_pred.keys()+d_real.keys()))
        a_pred = ev.convert_to_vector(l_tot_geo, d_pred, key='avg', type='binary')
        # print vectors
        self.assertTrue(self.isBinaryArray(a_pred))


    def isBinaryArray(self, a_val):
        for v in a_val[0]:
            if v not in [0,1]:
                print v
                return False
        return True


if __name__ == '__main__':
    unittest.main()