#!/bin/env python
# -*- coding: utf-8 -*-

import MySQLdb
import sys
# Set logging info
import logging
logging.basicConfig()
dbLogger = logging.getLogger(__name__)
dbLogger.setLevel(logging.INFO)

# Global Variables
con = None
cursor = None

# Set data conversion
from MySQLdb.constants import FIELD_TYPE
my_conv = { FIELD_TYPE.LONG: int, \
			FIELD_TYPE.DECIMAL: float }


def createConnection(dbDict):
	# load dictionary info
	hostname = dbDict['hostname']
	username = dbDict['username']
	password = dbDict['password']
	dbname = dbDict['dbname']
	# try connection
	try:
		global con
		con = MySQLdb.connect(hostname, username, password, dbname, conv=my_conv)
		global cursor
		cursor = con.cursor()
		# 
		resData = make_query("SELECT VERSION()")
		dbLogger.info(' MySQL Connection Succeed (version: %s)' %resData[0] )
	except MySQLdb.Error, e:
		dbLogger.info(' Connection Error %d: %s' % (e.args[0], e.args[1]) )
		sys.exit(1)

def closeDB():
	global con
	con.close()
	dbLogger.info(' DB closed.')

def make_query(textQuery):
	try:
		global con
		# con.query( textQuery )
		# return con.use_result()
		cursor.execute( textQuery )
		return cursor.fetchall()
	except MySQLdb.Error, e:
		dbLogger.info(' Query Error %d: %s' % (e.args[0], e.args[1]) )
		sys.exit(1)

