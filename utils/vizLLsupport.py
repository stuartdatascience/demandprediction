#!/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

import geohash


__author__ = 'trevi'


def string_to_date(date_str):
    return datetime.strptime(date_str, "%d/%m/%Y %H:%M%p")


GEOHASHROW = 'TYPE_GEOHASH = L.rectangle(BOUNDARIES, {color: \'COLOR\', weight: VALUE}).bindPopup("VALUE TYPE requests")'
S2ROW = 'TYPE_S2 = L.circle([LATITUDE, LONGITUDE], 500, {\n\
    \tcolor: "COLOR",\n\
    \tfillColor: "COLOR",\n\
    \tfillOpacity: 0.5\n\
}).bindPopup("PVALUE predicted, RVALUE real")' # red: #f03
GROUP = 'var GROUPNAME = L.layerGroup([LIST]);\n\n'
OVERLAY = 'var overlayMaps = {\n\
ROW\n\
};\n\n'

class DrawGeohashJS:
    def __init__(self):
        self.out = ''
        self.overlay = ''

    def get_bbox_from_geohash(self, geo):
        d_bbox = geohash.bbox(geo)
        return '[ [%f,%f], [%f,%f] ]' %(d_bbox['n'],d_bbox['e'],d_bbox['s'],d_bbox['w'])

    def structurize(self, pred, demand):
        d_struct = {}
        for g in pred:
            d_struct[g] = {
                'color': '#045a8d',
                'pvalue': pred[g]['avg'],
                'rvalue': 0,
                'type': 'prediction',
                'lat': pred[g]['lat'],
                'lon': pred[g]['lon']
            }
        # check and add real
        for g in demand:
            if g not in d_struct:
                d_struct[g] = {
                    'color': '#b30000',
                    'pvalue': 0,
                    'rvalue': demand[g]['demand'],
                    'type': 'real',
                    'lat': demand[g]['lat'],
                    'lon': demand[g]['lon']
                }
            else:
                d_struct[g]['color'] = '#00b300'
                d_struct[g]['rvalue'] = demand[g]['demand']
        return d_struct


    def add_group(self, pred_date_str, pred, demand):
        if len(pred) == 0 and len(demand) == 0:
            return ''
        # structure the dictionaries
        d_struct = self.structurize(pred, demand)
        l_geohash = []
        out_group = 'var '
        out_csv = ''
        #
        for g in d_struct:
            type_name = d_struct[g]['type']
            out_group += '%s,\n' %self.build_row(d_struct, g, type_name)
            l_geohash.append( '%s_%s'%(type_name,g) )
            # write csv file
            if d_struct[g]['pvalue'] > 0:
                out_csv += '%f,%f,%f\n' %(d_struct[g]['lat'], d_struct[g]['lon'], d_struct[g]['pvalue'])
        if '\n' in out_group:
            # replace last comma
            out_group = out_group[:-2] +';\n\n'
            # get name
            datename = self.get_geo_name(pred_date_str)
            # add group line
            out_group += GROUP\
                .replace('GROUPNAME', datename)\
                .replace('LIST', ','.join(l_geohash))
            #
            if self.overlay != '':
                self.overlay += ',\n'
            self.overlay += '\t\'GROUPNAME\': GROUPNAME'.replace('GROUPNAME', datename)
            #
            return out_group, out_csv
        return ''

    def return_overlay(self):
        return OVERLAY.replace('ROW', self.overlay)

    def get_geo_name(self, pred_date_str):
        dd = string_to_date(pred_date_str)
        return 'time%s' %dd.strftime('%H%p')

    def build_row(self, d_struct, g, type_name):
        return self.s2_row(d_struct, g, type_name)

    def s2_row(self, d_struct, g, type_name):
        lat = str(d_struct[g]['lat'])
        lon = str(d_struct[g]['lon'])
        return S2ROW\
            .replace('TYPE', type_name)\
            .replace('S2', g)\
            .replace('COLOR', d_struct[g]['color'])\
            .replace('LATITUDE', lat)\
            .replace('LONGITUDE', lon)\
            .replace('PVALUE', str(d_struct[g]['pvalue']))\
            .replace('RVALUE', str(d_struct[g]['rvalue']))
