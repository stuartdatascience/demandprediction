# Demand Prediction #

This repository contains the code to predict the upcoming demand given a time range.

### Content ###

* `simpleDemandPrediction.py`: algorithm for the naive prediction approach
* `conf/`: contains configuration file and script for parsing it
* `dbutils/`: contains DB script files for querying the DB

### Detailed Information ###

Check the following [Positioning/Prediction](https://stuart-team.atlassian.net/wiki/pages/viewpage.action?pageId=1736918)  Confluence Page